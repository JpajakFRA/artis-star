INCLUDE_DIRECTORIES(
        ${ARTIS_BINARY_DIR}/src
        ${ARTIS_SOURCE_DIR}/src
        ${Boost_INCLUDE_DIRS})

LINK_DIRECTORIES(
        ${Boost_LIBRARY_DIRS})

SET(PDEVS_HPP Context.hpp Coordinator.hpp Dynamics.hpp GraphManager.hpp Simulator.hpp)

INSTALL(FILES ${PDEVS_HPP} DESTINATION ${ARTIS_INCLUDE_DIRS}/kernel/pdevs)

ADD_SUBDIRECTORY(multithreading)
ADD_SUBDIRECTORY(mpi)
