INCLUDE_DIRECTORIES(
        ${ARTIS_BINARY_DIR}/src
        ${ARTIS_SOURCE_DIR}/src
        ${Boost_INCLUDE_DIRS})

LINK_DIRECTORIES(
        ${Boost_LIBRARY_DIRS})

SET(COMMON_UTILS_HPP FormalismType.hpp FunctionType.hpp LevelType.hpp Multithreading.hpp String.hpp Trace.hpp)

INSTALL(FILES ${COMMON_UTILS_HPP} DESTINATION
        ${ARTIS_INCLUDE_DIRS}/common/utils)